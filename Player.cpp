#include <iostream>
#include "Player.h"

void Player::initVariables() {
    this->currLevel = 0;
    this->lives = 6;
}

void Player::initTextures() {
    for (int i = 0; i < 6; i++) {
        std::string path = "resources/textures/" + std::to_string(i) + ".png";
        if (!textures[i].loadFromFile(path))
            std::cout << "Loading texture failed, nr: " << path << std::endl;
    }
}

Player::Player() {
    this->initVariables();
    this->initTextures();
    this->createSprite();

    this->sprite.setOrigin(this->sprite.getLocalBounds().width / 2.f, this->sprite.getLocalBounds().height);
    this->sprite.setPosition(200,505);
}

Player::~Player() {

}

void Player::createSprite() {
    this->sprite.setTexture(this->textures[currLevel]);
}

void Player::respawn() {
    currLevel = 0;
    lives = 6;
    this->createSprite();
}

void Player::update() {
    this->currLevel++;
    this->lives--;
    this->createSprite();
}

void Player::render(sf::RenderTarget *target) {
    target->draw(this->sprite);
}
