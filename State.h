#ifndef HANGMAN_STATE_H
#define HANGMAN_STATE_H

#include <iostream>
#include <stack>
#include <map>
#include <vector>

#include <SFML/Graphics.hpp>

class State {
private:

protected:
    sf::RenderWindow *window;
    // Allows us to move between different states //
    std::stack<State *>* states;
    bool quit;
    sf::Event* sfEvent;

    sf::Vector2f mousePosition;

public:
    State(sf::RenderWindow *window, std::stack<State *>* states, sf::Event *e);
    virtual ~State();

    const bool &getQuit() const;

    static void setupText(sf::Text *textItem, const sf::Font &font, const sf::String &value, int size, sf::Color colour);

    virtual void checkForQuit();
    virtual void endState() = 0;

    virtual void updateMousePosition();
    virtual void updateInput() = 0;
    virtual void update() = 0;

    virtual void render(sf::RenderTarget *target) = 0;
};


#endif //HANGMAN_STATE_H
