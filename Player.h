#ifndef HANGMAN_PLAYER_H
#define HANGMAN_PLAYER_H

#include <SFML/Graphics.hpp>

class Player {
private:
    sf::Texture textures[6];
    sf::Sprite sprite;


    void initVariables();
    void initTextures();
public:
    short unsigned currLevel;
    short unsigned lives;

    Player();
    ~Player();

    void createSprite();
    void respawn();

    void update();
    void render(sf::RenderTarget* target);
};


#endif //HANGMAN_PLAYER_H
