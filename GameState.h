#ifndef HANGMAN_GAMESTATE_H
#define HANGMAN_GAMESTATE_H

#include "MenuState.h"
#include "Player.h"
#include "Button.h"
#include <vector>

enum GameStage {
    GAMEOVER = 0,
    RUNNING,
    WIN
};

class GameState : public State {
private:
    sf::RectangleShape background;
    Player *player;

    sf::Font font;
    sf::Text dashedText;
    sf::Text livesText;
    sf::Text answerText;
    sf::Text gameOverText;
    sf::Text roundWinText;
    sf::Text nextRoundText;

    std::string currentWord;
    std::string dashedWord;
    std::vector<std::string> wordlist;
    short unsigned currentGameStage;

    std::map<char, int> letterClickCounter;

    std::map<std::string, Button *> buttons;

    void initFonts();

    void initWordlist();

    void initText();

    void initButtons();

public:
    GameState(sf::RenderWindow *window, std::stack<State *> *states, sf::Event *e);

    ~GameState() override;

    void endState() override;

    void getRandomWord();

    void updateInput() override;

    void updateButtons();

    void update() override;

    void renderButtons(sf::RenderTarget *target = nullptr);

    void render(sf::RenderTarget *target);
};


#endif //HANGMAN_GAMESTATE_H
