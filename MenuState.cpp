#include "MenuState.h"
#include "GameState.h"

void MenuState::initFonts() {
    if(!this->font.loadFromFile("resources/fonts/Silkscreen.ttf")){
        std::cout<<"ERROR::MAINMENUSTATE::COULD NOT LOAD FONT"<<std::endl;
    }
}

void MenuState::initTitle() {
    this->title.setString("HANGMAN");
    this->title.setFillColor(sf::Color::White);
    this->title.setFont(this->font);
    this->title.setCharacterSize(100);
    this->title.setOrigin(this->title.getLocalBounds().width/2.f, this->title.getLocalBounds().height/2.f);
    this->title.setPosition(500, 200);
}

void MenuState::initButtons() {
    this->buttons["GAME_STATE"] = new Button(300, 400, 200, 80, &this->font, "New Game",25, sf::Color::White, sf::Color(54, 194,54), sf::Color(54, 194,54, 200), sf::Color(20,20,20,200));

    this->buttons["QUIT_STATE"] = new Button(550, 400, 200, 80, &this->font, "Quit", 25, sf::Color::White, sf::Color(194, 54,54), sf::Color(194, 54,54, 200), sf::Color(20,20,20,200));
}

MenuState::MenuState(sf::RenderWindow *window, std::stack<State*>* states, sf::Event *e) : State(window, states, e) {
    this->initFonts();
    this->initTitle();
    this->initButtons();

    this->background.setSize(sf::Vector2f(static_cast<float>(window->getSize().x), static_cast<float>(window->getSize().y)));
    this->background.setFillColor(sf::Color(66,135,245));
}

MenuState::~MenuState() {
    for(auto & button : this->buttons){
        delete button.second;
    }
}

void MenuState::endState() {
    std::cout << "Ending menu state" << std::endl;
}

void MenuState::updateInput() {
    sf::Event event;
    while (this->window->pollEvent(event)) {
        switch (event.type) {
            case sf::Event::Closed:
                window->close();
                break;
        }
    }
}

void MenuState::updateButtons() {
    for(auto &it:this->buttons){
        it.second->update(this->mousePosition);
    }

    if(buttons["GAME_STATE"]->isPressed()){
        this->states->push(new GameState(this->window, this->states, sfEvent));
    }

    if(buttons["QUIT_STATE"]->isPressed()){
        this->quit = true;
    }

}

void MenuState::update() {
    this->updateMousePosition();
    this->updateInput();
    this->updateButtons();
}

void MenuState::renderButtons(sf::RenderTarget *target) {
    for(auto &it:this->buttons){
        it.second->render(target);
    }
}

void MenuState::render(sf::RenderTarget *target) {
    target->draw(this->background);
    target->draw(this->title);
    this->renderButtons(this->window);
}