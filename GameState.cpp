#include "GameState.h"
#include <cctype>
#include <fstream>
#include <random>

void GameState::initFonts() {
    if (!this->font.loadFromFile("resources/fonts/Silkscreen.ttf")) {
        std::cout << "ERROR::MAINMENUSTATE::COULD NOT LOAD FONT" << std::endl;
    }
}

void GameState::initWordlist() {
    std::ifstream file("resources/wordlist.txt");
    std::string str;
    while (std::getline(file, str)) {
        // Converting strings to lower case
        std::for_each(str.begin(), str.end(), [](char &c) {
            c = static_cast<char>(::tolower(c));
        });
        this->wordlist.push_back(str);
    }
}

void GameState::initText() {
    setupText(&this->gameOverText, this->font, "GAME OVER", 70, sf::Color::Red);
    setupText(&this->roundWinText, this->font, "WIN", 70, sf::Color::Green);
    setupText(&this->nextRoundText, this->font, "Press ENTER to continue", 50, sf::Color::Black);

    setupText(&this->dashedText, this->font, "", 30, sf::Color::Black);

    setupText(&this->dashedText, this->font, "", 30, sf::Color::Black);
    setupText(&this->livesText, this->font, "Lives: 6", 30, sf::Color::Black);
    setupText(&this->answerText, this->font, "", 30, sf::Color::Black);

    this->gameOverText.setOrigin(this->gameOverText.getLocalBounds().width / 2.f, this->gameOverText.getLocalBounds().height / 2.f);
    this->roundWinText.setOrigin(this->roundWinText.getLocalBounds().width / 2.f, this->roundWinText.getLocalBounds().height / 2.f);
    this->nextRoundText.setOrigin(this->nextRoundText.getLocalBounds().width / 2.f, this->nextRoundText.getLocalBounds().height / 2.f);

    sf::Vector2u winSize = this->window->getSize();
    this->gameOverText.setPosition(winSize.x / 2.f, 250);
    this->roundWinText.setPosition(winSize.x / 2.f, 250);
    this->nextRoundText.setPosition(winSize.x / 2.f, 340);
    this->livesText.setPosition(20, 20);
}

void GameState::initButtons() {
    std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
    int i = 0;
    int j = 0;
    for (char &c: alphabet) {
        std::string tmp;
        tmp += c;
        this->letterClickCounter[c] = 0;

        this->buttons[tmp] = new Button(500 + 65 * i, 300 + 65 * j + 10, 50, 50, &this->font, tmp, 30, sf::Color::White, sf::Color(194, 54, 54),
                                        sf::Color(194, 54, 54, 200),
                                        sf::Color(20, 20, 20, 200));
        i++;
        if (i % 7 == 0) {
            i = 0;
            j++;
        }
    }

}

GameState::GameState(sf::RenderWindow *window, std::stack<State *> *states, sf::Event *e) : State(window, states, e) {
    this->initFonts();
    this->initWordlist();
    this->initText();
    this->initButtons();
    this->getRandomWord();

    this->player = new Player();
    this->background.setSize(sf::Vector2f(static_cast<float>(window->getSize().x), static_cast<float>(window->getSize().y)));
    this->background.setFillColor(sf::Color(66, 135, 245));
    currentGameStage = GameStage::RUNNING;
    dashedWord = std::string(currentWord.size(), '_');

    livesText.setString("Lives: 6");
    dashedText.setString(dashedWord);

    sf::FloatRect dashedTextBounds = dashedText.getLocalBounds();
    dashedText.setOrigin(dashedTextBounds.left + dashedTextBounds.width / 2.0f, dashedTextBounds.top + dashedTextBounds.height / 2.0f);
    dashedText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y - 50));

}

GameState::~GameState() {
    delete this->player;
    auto it = this->buttons.begin();
    for (it = this->buttons.begin(); it != this->buttons.end(); ++it) {
        delete it->second;
    }
}

void GameState::endState() {
    std::cout << "Ending game state" << std::endl;
}

void GameState::getRandomWord() {
    int randomNumber = static_cast<int>(std::rand() % wordlist.size());
    currentWord = wordlist[randomNumber];

}

void GameState::updateInput() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) && (currentGameStage == GameStage::WIN || currentGameStage == GameStage::GAMEOVER)) {
        this->states->pop();
        this->states->push(new GameState(this->window, this->states, sfEvent));
    }

    sf::Event e;
    while (window->pollEvent(e)) {
        switch (e.type) {
            case sf::Event::Closed:
                window->close();
                break;
            case sf::Event::TextEntered:
                if (e.text.unicode > 32 && e.text.unicode < 128 && currentGameStage != GameStage::GAMEOVER) {
                    char pressedChar = std::tolower(static_cast<char>(e.text.unicode));
                    this->letterClickCounter[pressedChar]++;
                    std::size_t position = currentWord.find(pressedChar);
                    std::string tmp(1, pressedChar);
                    buttons.at(tmp)->setState(ButtonStates::BTN_PRESSED);
                    buttons.at(tmp)->checked = 1;
                    if (this->letterClickCounter[pressedChar] <= 1) {
                        if (position != std::string::npos) {
                            while (position != std::string::npos) {
                                dashedWord.replace(position, 1, 1, pressedChar);
                                position = currentWord.find(pressedChar, position + 1);
                            }
                            if (std::count(dashedWord.begin(), dashedWord.end(), '_') == 0) {
                                currentGameStage = GameStage::WIN;
                                answerText.setString("Answer: " + currentWord);
                                sf::FloatRect answerTextBounds = answerText.getLocalBounds();
                                answerText.setOrigin(answerTextBounds.left + answerTextBounds.width / 2.0f,
                                                     answerTextBounds.top + answerTextBounds.height / 2.0f);
                                answerText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y / 2.0f + 100));
                            }

                            dashedText.setString(dashedWord);
                            sf::FloatRect dashedTextBounds = dashedText.getLocalBounds();
                            dashedText.setOrigin(dashedTextBounds.left + dashedTextBounds.width / 2.0f,
                                                 dashedTextBounds.top + dashedTextBounds.height / 2.0f);
                            dashedText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y - 50));
                        } else {
                            player->update();
                            livesText.setString("Lives: " + std::to_string(player->lives));
                            if (player->lives <= 0) {
                                currentGameStage = GameStage::GAMEOVER;
                                answerText.setString("Answer: " + currentWord);
                                sf::FloatRect answerTextBounds = answerText.getLocalBounds();
                                answerText.setOrigin(answerTextBounds.left + answerTextBounds.width / 2.0f,
                                                     answerTextBounds.top + answerTextBounds.height / 2.0f);
                                answerText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y / 2.0f + 100));
                            }
                        }
                    }
                }

                break;
        }
    }


}

void GameState::updateButtons() {
    for (const auto &myPair: this->buttons) {
        myPair.second->update(this->mousePosition);
        if (myPair.second->isPressed() && myPair.second->checked == 0) {
            myPair.second->checked = 1;
            if (currentGameStage != GameStage::GAMEOVER) {
                char pressedChar = std::tolower(myPair.first[0]);
                std::size_t position = currentWord.find(pressedChar);
                if (position != std::string::npos) {
                    while (position != std::string::npos) {
                        dashedWord.replace(position, 1, 1, pressedChar);
                        position = currentWord.find(pressedChar, position + 1);
                    }
                    if (std::count(dashedWord.begin(), dashedWord.end(), '_') == 0) {
                        currentGameStage = GameStage::WIN;
                        answerText.setString("Answer: " + currentWord);
                        sf::FloatRect answerTextBounds = answerText.getLocalBounds();
                        answerText.setOrigin(answerTextBounds.left + answerTextBounds.width / 2.0f,
                                             answerTextBounds.top + answerTextBounds.height / 2.0f);
                        answerText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y / 2.0f + 100));
                    }

                    dashedText.setString(dashedWord);
                    sf::FloatRect dashedTextBounds = dashedText.getLocalBounds();
                    dashedText.setOrigin(dashedTextBounds.left + dashedTextBounds.width / 2.0f,
                                         dashedTextBounds.top + dashedTextBounds.height / 2.0f);
                    dashedText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y - 50));
                } else {
                    player->update();
                    livesText.setString("Lives: " + std::to_string(player->lives));
                    if (player->lives <= 0) {
                        currentGameStage = GameStage::GAMEOVER;
                        answerText.setString("Answer: " + currentWord);
                        sf::FloatRect answerTextBounds = answerText.getLocalBounds();
                        answerText.setOrigin(answerTextBounds.left + answerTextBounds.width / 2.0f,
                                             answerTextBounds.top + answerTextBounds.height / 2.0f);
                        answerText.setPosition(sf::Vector2f(window->getSize().x / 2.0f, window->getSize().y / 2.0f + 100));
                    }
                }
            }
        }
    }
}

void GameState::update() {
    this->updateMousePosition();
    this->updateInput();
    this->updateButtons();
}

void GameState::renderButtons(sf::RenderTarget *target) {
    if (!target) {
        target = this->window;
    }
    for (const auto &myPair: this->buttons) {
        myPair.second->render(target);
    }
}

void GameState::render(sf::RenderTarget *target) {
    target->draw(this->background);

    switch (currentGameStage) {
        case RUNNING:
            target->draw(dashedText);
            target->draw(livesText);
            this->player->render(target);
            this->renderButtons();
            break;
        case GAMEOVER:
            target->draw(answerText);
            target->draw(gameOverText);
            target->draw(nextRoundText);
            break;
        case WIN:
            target->draw(answerText);
            target->draw(roundWinText);
            target->draw(nextRoundText);
            break;
    }

}
