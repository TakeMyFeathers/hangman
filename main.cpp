#include "Game.h"
#include <cstdlib>
#include <ctime>

int main() {
    srand(time(NULL));

    Game engine;
    engine.run();

    return 0;
}