#include "Button.h"
#include <iostream>

Button::Button(float x, float y, float width, float height, sf::Font *font,
               std::string text, unsigned int characterSize, sf::Color textColor, sf::Color idleColor, sf::Color hoverColor, sf::Color pressedColor) {

    this->buttonState = ButtonStates::BTN_IDLE;

    this->shape.setSize(sf::Vector2f(width, height));
    this->shape.setPosition(sf::Vector2f(x, y));
    this->font = font;
    this->characterSize = characterSize;
    this->textColor = textColor;
    this->text.setFont(*this->font);
    this->text.setString(text);
    this->text.setFillColor(textColor);
    this->text.setCharacterSize(characterSize);
    this->text.setOrigin(this->text.getLocalBounds().width / 2.f, this->text.getLocalBounds().height / 2.f);
    this->text.setPosition(
            this->shape.getPosition().x + (this->shape.getLocalBounds().width / 2.f),
            this->shape.getPosition().y + (this->shape.getLocalBounds().height / 2.f)-(this->text.getLocalBounds().height/2.f)
    );

    this->idleColor = idleColor;
    this->hoverColor = hoverColor;
    this->pressedColor = pressedColor;

    this->shape.setFillColor(this->idleColor);
}

Button::~Button() = default;

bool Button::isPressed() const {
    if (this->buttonState == ButtonStates::BTN_PRESSED) {
        return true;
    }
    return false;
}

void Button::setState(unsigned short state) {
    this->buttonState=state;
    switch (this->buttonState) {
        case ButtonStates::BTN_IDLE:
            this->shape.setFillColor(this->idleColor);
            break;
        case ButtonStates::BTN_HOVER:
            this->shape.setFillColor(this->hoverColor);
            break;
        case ButtonStates::BTN_PRESSED:
            this->shape.setFillColor(this->pressedColor);
            break;
        default:
            this->shape.setFillColor(sf::Color::Red);
            break;
    }
}

void Button::update(sf::Vector2f mousePos) {
    // Skip everything if button was pressed before //
    if (this->buttonState == ButtonStates::BTN_PRESSED && checked==1) {
        return;
    }

    // Set current button state //
    this->buttonState = ButtonStates::BTN_IDLE;
    if (this->shape.getGlobalBounds().contains(mousePos)) {
        this->buttonState = ButtonStates::BTN_HOVER;
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            this->buttonState = ButtonStates::BTN_PRESSED;
        }
    }

    // Update button color //
    switch (this->buttonState) {
        case ButtonStates::BTN_IDLE:
            this->shape.setFillColor(this->idleColor);
            break;
        case ButtonStates::BTN_HOVER:
            this->shape.setFillColor(this->hoverColor);
            break;
        case ButtonStates::BTN_PRESSED:
            this->shape.setFillColor(this->pressedColor);
            break;
        default:
            this->shape.setFillColor(sf::Color::Red);
            break;
    }
}

void Button::render(sf::RenderTarget *target) {
    target->draw(this->shape);
    target->draw(this->text);
}
