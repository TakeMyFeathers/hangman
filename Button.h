#ifndef HANGMAN_BUTTON_H
#define HANGMAN_BUTTON_H

#include <SFML/Graphics.hpp>

enum ButtonStates {
    BTN_IDLE = 0,
    BTN_HOVER,
    BTN_PRESSED
};

class Button {
private:
    short unsigned buttonState;

    sf::RectangleShape shape;
    sf::Font *font;
    sf::Text text;
    unsigned int characterSize;
    sf::Color textColor;

    sf::Color idleColor;
    sf::Color hoverColor;
    sf::Color pressedColor;

public:
    bool checked = 0;


    Button(float x, float y, float width, float height, sf::Font *font,
           std::string text, unsigned int characterSize, sf::Color textColor, sf::Color idleColor, sf::Color hoverColor, sf::Color pressedColor);

    ~Button();

    void setState(short unsigned state);

    bool isPressed() const;

    void update(sf::Vector2f mousePos);

    void render(sf::RenderTarget *target);
};


#endif //HANGMAN_BUTTON_H
