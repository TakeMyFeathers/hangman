#ifndef HANGMAN_GAME_H
#define HANGMAN_GAME_H

#include <SFML/Graphics.hpp>
#include "MenuState.h"

class Game {
private:
    sf::RenderWindow *window;
    sf::Event sfEvent;

    std::stack<State *> states;

    // Creates an SFML window with settings specified in window.ini config file //
    void initWindow();

    // Adds MenuState to the stack of states //
    void initStates();

public:
    Game();

    virtual ~Game();

    void updateSFMLEvents();

    // Handle changes in the application //
    void update();

    // Render current state that is on the top of the stack //
    void render();

    void run();


};

#endif //HANGMAN_GAME_H
