#ifndef HANGMAN_MENUSTATE_H
#define HANGMAN_MENUSTATE_H

#include "State.h"
#include "Button.h"

class MenuState : public State {
private:
    sf::RectangleShape background;
    sf::Font font;
    sf::Text title;

    std::map<std::string, Button *> buttons;

    void initFonts();
    void initTitle(); // Probably rework
    void initButtons();

public:
    MenuState(sf::RenderWindow *window, std::stack<State*>* states, sf::Event *e);

    ~MenuState() override;

    void endState() override;

    // Handle user keyboard input //
    void updateInput() override;

    // Handle button press //
    void updateButtons();

    void update() override;

    void renderButtons(sf::RenderTarget *target);

    void render(sf::RenderTarget *target) override;
};


#endif //HANGMAN_MENUSTATE_H
