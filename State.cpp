#include "State.h"

State::State(sf::RenderWindow *window, std::stack<State*>* states, sf::Event* e) {
    this->window = window;
    this->states = states;
    this->sfEvent = e;
    this->quit = false;
}

State::~State() = default;

void State::setupText(sf::Text *textItem, const sf::Font &font, const sf::String &value, int size, sf::Color colour) {
    textItem->setFont(font);
    textItem->setString(value);
    textItem->setCharacterSize(size);
    textItem->setFillColor(colour);
}


void State::updateMousePosition() {
    this->mousePosition = this->window->mapPixelToCoords(sf::Mouse::getPosition(*this->window));
}

const bool &State::getQuit() const {
    return this->quit;
}

void State::checkForQuit() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        this->quit = true;
    }
}
