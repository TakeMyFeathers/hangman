#include <iostream>
#include <fstream>
#include "Game.h"

void Game::initWindow() {
    std::ifstream ifs("config/window.ini");

    std::string windowTitle = "Window";
    sf::VideoMode windowBounds(1000, 800);
    unsigned int framerateLimit = 60;

    if (ifs.is_open()) {
        std::getline(ifs, windowTitle);
        ifs >> windowBounds.width >> windowBounds.height;
        ifs >> framerateLimit;
    }

    this->window = new sf::RenderWindow(windowBounds, windowTitle, sf::Style::Close);
    this->window->setFramerateLimit(framerateLimit);

    sf::Image programIcon;
    if (!programIcon.loadFromFile("resources/toucan.png"))
        std::cout << "Font, loading failed" << std::endl;
    window->setIcon(programIcon.getSize().x, programIcon.getSize().y, programIcon.getPixelsPtr());

}

void Game::initStates() {
    this->states.push(new MenuState(this->window, &this->states, &sfEvent));
}

Game::Game() {
    this->initWindow();
    this->initStates();
}

Game::~Game() {
    delete this->window;
    while (!this->states.empty()) {
        delete this->states.top();
        this->states.pop();
    }
}

void Game::updateSFMLEvents() {
    sf::Event event;
    while (this->window->pollEvent(event)) {
        switch (event.type) {
            case sf::Event::Closed:
                window->close();
                break;
        }
    }
}

void Game::update() {
    if (!this->states.empty()) {
        this->states.top()->update();
        this->states.top()->updateInput();
        if (this->states.top()->getQuit()) {
            this->states.top()->endState();
            delete this->states.top();
            this->states.pop();
        }
    }
    // Application end //
    else {
        std::cout << "Closing application";
        this->window->close();
    }


}

void Game::render() {
    this->window->clear(sf::Color::White);

    if (!this->states.empty()) {
        this->states.top()->render(this->window);
    }

    this->window->display();
}

void Game::run() {
    while (this->window->isOpen()) {
        this->update();
        this->render();
    }
}
